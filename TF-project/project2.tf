provider "aws" {}
#All variables are pretty descriptive from the naming
#All components will have Name-prefix(Dev-prefix) on production environment that will ahve proper prefix and so on.

variable vpc_cidr_block{}
variable subnet_cidr_block{}
variable avail_zone{}
variable env_prefix{}
variable my_ip{}

#String interpolation that's basically having variable value and string glued together. In order to use variable value we are going do doller
#sign ($) and curly braces {}. So using variable ouside, not inside the string or inside the quotes "" is var  variable name. 
#If we want to use the variable inside a string bcz we ant to glue it or put it togther with another string , so we use ${} and inside we can 
#do var and the variable name.
#This how we can use variable inside the strings in tags. (Name: "${var.env_prefix}-vpc") 

resource "aws_vpc" "MY-VPC" {
    cidr_block = var.vpc_cidr_block
    tags={
        Name: "${var.env_prefix}-vpc"
    }
}

resource "aws_subnet" "Pub-subnet-1"{
    vpc_id= aws_vpc.MY-VPC.id
    cidr_block = var.subnet_cidr_block
    availability_zone = var.avail_zone
    tags={
        Name: "${var.env_prefix}-subnet-1"
    }
}

#Route-Table: it is like a virtual router in your private network, R-t will basically decide where the traffic will be forwarded to within the VPC.
#AWS will crate R_T whenever create a VPC by defaultbc each VPC will need one.
#NACL is basically a firewall configuration for our VPC (Subnet) which applies for Subnets in that VPC.
#NACL doesnt actually restrick any traffic. It is open by default.
#S.G which is a firewall configuration fo servers. on server level and it's also has inbound and outbound rules and by default erverything is open.
#S.G actually restrick any traffic. It is closed by default.
 
resource "aws_internet_gateway" "MY-IGW"{
    vpc_id = aws_vpc.MY-VPC.id
    tags = {
          Name: "${var.env_prefix}-igw"

    }
}

resource "aws_route_table" "MY-RTB" {
   vpc_id = aws_vpc.MY-VPC.id
   route {
          cidr_block = "0.0.0.0/0"
          gateway_id = aws_internet_gateway.MY-IGW.id
         }
   tags ={
          Name: "${var.env_prefix}-rtb"
         }     
}

#for create
#I we want to use the existing default S.G bcs it comes out of the box when we create new box. But still creating one is recommended in most cases.
 #the syntax of configuring default S.G instead of creating new one.
 #Use the default S.G:
 #It is very easy to switch from new sg to default s.G
 
 resource "aws_default_security_group" "default-sg" {
    vpc_id = aws_vpc.MY-VPC.id

    ingress {
     from_port = 22
     to_port = 22
     protocol = "tcp"
     cidr_blocks = [var.my_ip]
    }

    ingress {
     from_port = 8080
     to_port = 8080
     protocol = "tcp"
     cidr_blocks = ["0.0.0.0/0"]
    }
    #This code will map to o/b entry which basically allow all ports and all protocols all port ranges for all IP-Addes, 
    #this what we confgured with these attributes
    egress {
     from_port = 0
     to_port = 0
     #0 means any port
     protocol = "-1"
     # we don't want to restrict the protocol to any IP-Add
     cidr_blocks = ["0.0.0.0/0"]
     prefix_list_ids = []
     #It is just allowing access to VPC endpoints.
    }
    
    tags ={
          Name: "${var.env_prefix}-default-sg"
         }
  
  #This will just allow any traffic to leave the VPC and The server itself, and this will configure all the rules and S.G
 }





