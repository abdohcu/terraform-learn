provider "aws" {}
#All variables are pretty descriptive from the naming
#All components will have Name-prefix(Dev-prefix) on production environment that will ahve proper prefix and so on.

variable vpc_cidr_block{}
variable subnet_cidr_block{}
variable avail_zone{}
variable env_prefix{}
variable my_ip{}
variable instance_type {}
variable public_key_location {}

#String interpolation that's basically having variable value and string glued together. In order to use variable value we are going do doller
#sign ($) and curly braces {}. So using variable ouside, not inside the string or inside the quotes "" is var  variable name. 
#If we want to use the variable inside a string bcz we ant to glue it or put it togther with another string , so we use ${} and inside we can 
#do var and the variable name.
#This how we can use variable inside the strings in tags. (Name: "${var.env_prefix}-vpc") 

resource "aws_vpc" "MY-VPC" {
    cidr_block = var.vpc_cidr_block
    tags={
        Name: "${var.env_prefix}-vpc"
    }
}

resource "aws_subnet" "Pub-subnet-1"{
    vpc_id= aws_vpc.MY-VPC.id
    cidr_block = var.subnet_cidr_block
    availability_zone = var.avail_zone
    tags={
        Name: "${var.env_prefix}-subnet-1"
    }
}

#Route-Table: it is like a virtual router in your private network, R-t will basically decide where the traffic will be forwarded to within the VPC.
#AWS will crate R_T whenever create a VPC by defaultbc each VPC will need one.
#NACL is basically a firewall configuration for our VPC (Subnet) which applies for Subnets in that VPC.
#NACL doesnt actually restrick any traffic. It is open by default.
#S.G which is a firewall configuration fo servers. on server level and it's also has inbound and outbound rules and by default erverything is open.
#S.G actually restrick any traffic. It is closed by default.
 
resource "aws_internet_gateway" "MY-IGW"{
    vpc_id = aws_vpc.MY-VPC.id
    tags = {
          Name: "${var.env_prefix}-igw"

    }
}

resource "aws_route_table" "MY-RTB" {
   vpc_id = aws_vpc.MY-VPC.id
   route {
          cidr_block = "0.0.0.0/0"
          gateway_id = aws_internet_gateway.MY-IGW.id
         }
   tags ={
          Name: "${var.env_prefix}-rtb"
         }     
}

#for create
resource "aws_security_group" "myapp-sg" {
    name = "myapp-sg"
    vpc_id = aws_vpc.MY-VPC.id

    ingress {
     from_port = 22
     to_port = 22
     protocol = "tcp"
     cidr_blocks = [var.my_ip]
    }

    ingress {
     from_port = 8080
     to_port = 8080
     protocol = "tcp"
     cidr_blocks = ["0.0.0.0/0"]
    }
    #This code will map to o/b entry which basically allow all ports and all protocols all port ranges for all IP-Addes, 
    #this what we confgured with these attributes
    egress {
     from_port = 0
     to_port = 0
     #0 means any port
     protocol = "-1"
     # we don't want to restrict the protocol to any IP-Add
     cidr_blocks = ["0.0.0.0/0"]
     prefix_list_ids = []
     #It is just allowing access to VPC endpoints.
    }
    
    tags ={
          Name: "${var.env_prefix}-sg"
         }
  
  #This will just allow any traffic to leave the VPC and The server itself, and this will configure all the rules and S.G
 }

#we can refernece the ami from the data using data, the name of the data.

data "aws_ami" "latest-amazon-linux-image" {
    most_recent = true
    owners = ["099720109477"]
    filter {
       name = "name"
       values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-20230517"]
    }

    filter {
       name = "root-device-type"
       values = ["ebs"]
    }

    filter {
       name = "virtualization-type"
       values = ["hvm"]
    }
}

#we output the result of this query so that we can test whether our filter criteria are set corectly. to validate the result is getting 

/*output "aws_ami_id" {
#value = data.aws_ami.latest-amazon-linux-image.name
#value = data.aws_ami.latest-amazon-linux-image.id
# to get the id of the ami-image
}*/

resource "aws_key_pair" "ssh-key" {
 key_name = "server-key"
 #public_key = "${file(var.public_key_location)}"
 public_key = file(var.public_key_location)
 
 }

resource "aws_instance" "myapp-server" {
   ami = data.aws_ami.latest-amazon-linux-image.id
#However, this gives us the whole object, the whole image with all the attribuates.
   instance_type = var.instance_type
   subnet_id = aws_subnet.Pub-subnet-1.id 
   vpc_security_group_ids = [aws_security_group.myapp-sg.id]
   availability_zone = var.avail_zone 
   associate_public_ip_address = true
   key_name = aws_key_pair.ssh-key.key_name
   tags ={
          Name: "${var.env_prefix}-server"
         }

}


/*AMI:
How to set ami-id dynamically?
This means no need to hard-code as string this value by copying it. so instead we want actually to set it dynamically from AWS programmatically.
 In oder to fetch the existing resourses to basically query the data from AWS, we use not resource, but rather data type by creating data.
Filter in data, basically filter blocks are what let us define the criteria for these queries, it's just you are telling Amazon that give 
the most recent images that owned by Amazon that starts with this sting and ends with this, we dont care whatever is inside, 
in filter attribute we have name ( referencing which key we want to filter/ set criteria a name) and values( may be a list []  attributes for each filter,
 we can define many filters. In this way, we dont have to worry about hardcoding and updating the value manually.

Other arguments are optionals
if we dont specify it , it will be lanuched in default VPC  

Keypair:
mv ~/Downlaods/iam.pem ~/.ssh
to move pem key to ssh folder, this is scure place on your internet to store private keys
ls ~/.ssh/iam.epm 
ls -l ~/.ssh/iam.pem
chmod 400 ~/.ssh/iam.pem
To set the permission to be strickter , we just let the permission for the owner


To serach in terraform:
aws ec2 resource from terraform
data aws_ami_terraform resource*/





